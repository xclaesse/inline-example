#include <string.h>

#ifndef INSIDE_FOO_C
__attribute__((__dllexport__)) inline
#endif
extern
int g_strcmp0(const char*str1, const char*str2) {
  return strcmp(str1, str2);
}